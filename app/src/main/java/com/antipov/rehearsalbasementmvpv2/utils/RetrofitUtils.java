package com.antipov.rehearsalbasementmvpv2.utils;

import android.content.Context;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.antipov.rehearsalbasementmvpv2.utils.Constants.BASE_URL_SERVER;

/**
 * Created by antipov on 3/27/17.
 * there is two different static methods:
 * 1rst require context as input parameter for cookie interception
 * and writing it to the session
 *
 * 2nd is simple returning retrofit builder
 */

public class RetrofitUtils {
    public static Retrofit getApiClientWithInterceptor(final Context ctx){
        OkHttpClient cookieInterceptor = new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public Response intercept(Interceptor.Chain chain) throws IOException {
                                Response originalResponse = chain.proceed(chain.request());
                                if (!originalResponse.headers("Set-Cookie").isEmpty()) {
                                    SessionManager sessionManager = new SessionManager(ctx);
                                    sessionManager.logInUser(originalResponse.header("Set-Cookie"));
                                }
                                return originalResponse;
                            }
                        }).build();
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.create();
        return new Retrofit.Builder()
                .baseUrl(BASE_URL_SERVER)
                .client(cookieInterceptor)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(rxAdapter)
                .build();
    }

    public static Retrofit getApiBookingApiClient(){
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.create();
        return new Retrofit.Builder()
                .baseUrl(BASE_URL_SERVER)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(rxAdapter)
                .build();
    }

}
