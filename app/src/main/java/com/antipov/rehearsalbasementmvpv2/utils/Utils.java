package com.antipov.rehearsalbasementmvpv2.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by antipov on 13.02.17.
 */

public class Utils {
    public static void showMessage(Context ctx, String msg){
        Toast toast = Toast.makeText(ctx,
                msg, Toast.LENGTH_SHORT);
        toast.show();
    }

    public static void allowCancelDialog(Boolean isAllowed, AlertDialog dialog){
        dialog.setCancelable(isAllowed);
        Button buttonNegative = dialog.getButton(Dialog.BUTTON_NEGATIVE);
        Button buttonPositive = dialog.getButton(Dialog.BUTTON_POSITIVE);
        buttonNegative.setEnabled(isAllowed);
        buttonPositive.setEnabled(isAllowed);
    }

    public static String parseDate(String time){
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("GMT+3:00"));
        Date d = new Date(Long.valueOf(time));
        return format.format(d);
    }

    public static String parseTime(String time) {
        SimpleDateFormat format = new SimpleDateFormat("HH.mm", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("GMT+3:00"));
        Date d = new Date(Long.valueOf(time));
        return format.format(d);
    }
}
