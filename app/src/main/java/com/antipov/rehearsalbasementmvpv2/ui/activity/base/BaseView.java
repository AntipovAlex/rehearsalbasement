package com.antipov.rehearsalbasementmvpv2.ui.activity.base;

/**
 * Created by antipov on 28.03.17.
 */

public interface BaseView {
    interface View{
        void initPresenter();

        void initViews();
    }
}
