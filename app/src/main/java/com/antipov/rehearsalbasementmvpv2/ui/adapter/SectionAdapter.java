package com.antipov.rehearsalbasementmvpv2.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.antipov.rehearsalbasementmvpv2.R;
import com.antipov.rehearsalbasementmvpv2.model.BookingModel;
import com.antipov.rehearsalbasementmvpv2.utils.Utils;

import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

/**
 * Created by antipov on 10.04.17.
 */

public class SectionAdapter extends StatelessSection {
    private final String title;
    private final List<BookingModel> dataset;
    private final Context context;

    public SectionAdapter(String title, List<BookingModel> dataset, Context context) {
        super(R.layout.recycler_section, R.layout.bookings_list_card);
        this.title = title;
        this.dataset = dataset;
        this.context = context;
    }

    // views in one  card
    private static class ViewHolder extends RecyclerView.ViewHolder {
        TextView dateText;
        TextView timeText;
        TextView userComment;
        TextView adminComment;

        ViewHolder(View v) {
            super(v);
            dateText = (TextView) v.findViewById(R.id.mybooking_date_text);
            timeText = (TextView) v.findViewById(R.id.mybooking_time_text);
            userComment = (TextView) v.findViewById(R.id.mybooking_comment_text);
            adminComment = (TextView) v.findViewById(R.id.mybooking_admin_comment_text);

        }
    }

    //binding header
    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;

        HeaderViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.section_tex);
        }
    }

    @Override
    public int getContentItemsTotal() {
        return dataset.size();
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new ViewHolder(view);
    }

    //binding views
    @Override
    public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHolder itemHolder = (ViewHolder) holder;
        BookingModel v = dataset.get(position);
        String time = Utils.parseTime(v.getTime());
        String date = Utils.parseDate(v.getTime());

        if (v.getComment() == null || v.getComment().equals("")){
            itemHolder.userComment.setVisibility(View.GONE);
        } else {
            itemHolder.userComment.setText(
                    context.getString(R.string.bookings_comment_format, v.getComment()));
        }
        if (v.getAdminComment() == null || v.getAdminComment().equals("")){
            itemHolder.adminComment.setVisibility(View.GONE);
        } else {
            itemHolder.adminComment.setText(
                    context.getString(R.string.bookings_admin_comment_format,
                            v.getAdminComment()));
        }

        itemHolder.dateText.setText(context.getString(R.string.bookings_date_format, date));
        itemHolder.timeText.setText(context.getString(R.string.bookings_time_format, time));
    }


    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
        HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
        headerHolder.tvTitle.setText(title);
    }

    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
        return new HeaderViewHolder(view);
    }
}
