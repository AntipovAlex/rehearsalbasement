package com.antipov.rehearsalbasementmvpv2.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.antipov.rehearsalbasementmvpv2.ui.fragment.mybookings.MyBookingsPageFragment;
import com.antipov.rehearsalbasementmvpv2.utils.Constants;

/**
 * Created by antipov on 23.03.17.
 */

public class BookingsPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;

    public BookingsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return MyBookingsPageFragment.newInstance(Constants.PAST);
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return MyBookingsPageFragment.newInstance(Constants.TODAY);
            case 2: // Fragment # 1 - This will show SecondFragment
                return MyBookingsPageFragment.newInstance(Constants.INFUTURE);
            default:
                return null;
        }
    }


    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Прошедшие";
            case 1:
                return "Сегодня";
            case 2:
                return "Грядущие";
            default:
                return "getPageTitle err";
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
