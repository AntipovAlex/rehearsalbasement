package com.antipov.rehearsalbasementmvpv2.ui.fragment.booking;

import com.antipov.rehearsalbasementmvpv2.model.BookingModel;
import com.antipov.rehearsalbasementmvpv2.ui.activity.base.BaseView;

import java.util.List;

/**
 * Created by antipov on 3/19/17.
 */

public interface BookingView extends BaseView.View {

    void setDate(String date);

    void displayList();

    void showBookingDialog();

    interface BusLifecycle{
        void registerBus();

        void unRegisterBus();
    }

    interface OnBookingLoad {
        void onSuccessBookingLoad(List<BookingModel> bookings);

        void onFailureBookingLoad(String s);
    }
}
