package com.antipov.rehearsalbasementmvpv2.ui.dialog.booking;


import com.antipov.rehearsalbasementmvpv2.model.UserModel;
import com.antipov.rehearsalbasementmvpv2.ui.activity.navigation.NavigationActivity;
import com.antipov.rehearsalbasementmvpv2.utils.SessionManager;
import com.antipov.rehearsalbasementmvpv2.utils.Utils;

import java.util.Locale;
import java.util.Vector;

/**
 * Created by antipov on 3/20/17.
 */

public class BookingDialogPresenterImpl implements BookingDialogPresenter,
        BookingDialogInteractor.OnGetArgsFromBundle,
        BookingDialogInteractor.OnBook,
        BookingDialogInteractor.OnLoadUser,
BookingDialogInteractor.OnNotificationsUpdated{
    BookingDialogFragment view;
    BookingDialogInteractorImpl interactor;
    public BookingDialogPresenterImpl(BookingDialogFragment view) {
        this.view = view;
        interactor = new BookingDialogInteractorImpl();
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }

    @Override
    public void getArgsFromBundle() {
        interactor.getArgsFromBundle(view, this);
    }

    @Override
    public void book(String year, String month, String day, Vector<Integer> time,
                     String name, String telephone, String comment, String type) {
        String stringTime = "";
        for (Integer i : time) {
            //Locale.getDefault() - cause IDE worry about bugs with locale
            stringTime += String.format(Locale.getDefault(), "%02d",i)+",";
        }
        stringTime = stringTime.substring(0, stringTime.length()-1);
        interactor.book(view.getContext(), year, month, day, stringTime, name, telephone, comment, type, this);
    }

    @Override
    public void chekIsAuth() {
        SessionManager sessionManager = new SessionManager(view.getContext());
        if (sessionManager.isLoggedIn()){
            view.ifAuthed();
        } else {
            view.ifNotAuthed();
        }
    }

    @Override
    public void loadUser() {
        interactor.loadUser(view.getContext(), this);
    }

    @Override
    public void updateNotifications() {
        interactor.updateNotifications(view.getContext(), this);
    }

    @Override
    public void onGetArgsFromBundleSucces(String year, String month, String day, Vector<Integer> times) {
        view.setDate(year, month, day);
        view.setTimes(times);
    }

    @Override
    public void onGetArgsFromBundleFailure() {
        Utils.showMessage(view.getContext(), "Failure during getting data from bundle ...");
    }

    @Override
    public void onBookSuccess() {
        view.bookingSuccess();
    }

    @Override
    public void onBookFailure(String error) {
        view.bookingFailure(error);
    }

    @Override
    public void onLoadUserSucces(UserModel user) {
        view.onLoadUserSuccess(user);
    }

    @Override
    public void onLoadUserFailure(String s) {
        view.onLoadUserFailure(s);
    }

    @Override
    public void onNotificationUpdateSuccess(int i) {
        NavigationActivity navigationActivity = (NavigationActivity) view.getActivity();
        navigationActivity.onGetNotificationSuccess(i);
        view.dismiss();
    }

    @Override
    public void onNotificationUpdateFailure(String message) {
        NavigationActivity navigationActivity = (NavigationActivity) view.getActivity();
        navigationActivity.onGetNotificationFailure(message);
        view.dismiss();
    }
}
