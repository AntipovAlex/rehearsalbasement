package com.antipov.rehearsalbasementmvpv2.ui.dialog.booking;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.antipov.rehearsalbasementmvpv2.R;
import com.antipov.rehearsalbasementmvpv2.events.HidePreloader;
import com.antipov.rehearsalbasementmvpv2.events.ShowPreloader;
import com.antipov.rehearsalbasementmvpv2.model.UserModel;
import com.antipov.rehearsalbasementmvpv2.ui.fragment.booking.BookingFragment;
import com.antipov.rehearsalbasementmvpv2.ui.fragment.booking.BookingView;
import com.antipov.rehearsalbasementmvpv2.utils.Utils;
import com.github.pinball83.maskededittext.MaskedEditText;
import com.github.ybq.android.spinkit.SpinKitView;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Vector;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.antipov.rehearsalbasementmvpv2.utils.Constants.BOOKING_TYPE;
import static com.antipov.rehearsalbasementmvpv2.utils.Constants.REG_EXP_PHONE;
import static com.antipov.rehearsalbasementmvpv2.utils.Utils.allowCancelDialog;

/**
 * Created by antipov on 3/20/17.
 */

public class BookingDialogFragment extends DialogFragment implements BookingDialogView,
        Validator.ValidationListener, BookingDialogView.onBookingSend, BookingDialogView.onLoadUser,
        BookingView.BusLifecycle{
    private View view;
    private BookingDialogPresenter presenter;

    @BindView(R.id.spinnerType)
    Spinner spinner;

    @NotEmpty(message = "Поле обязательно для ввода")
    @BindView(R.id.editTextNameBooking)
    EditText nameEditText;

    @BindView(R.id.editTextCommentBooking)
    EditText commentEditText;

    @Pattern(regex = REG_EXP_PHONE, message =
            "Поле должно быть заполнено в формате 380ХХХХХХХХХ")
    @BindView(R.id.editTextPhoneNumber)
    MaskedEditText phoneNumberEdit;

    @BindView(R.id.textBookingTime2)
    TextView tv;

    @BindView(R.id.send_booking_spin_kit)
    SpinKitView sendBookingSpinKit;
    String year;
    String month;
    String day;

    Vector<Integer> times;

    private BookingFragment parentView;
    private boolean validated;
    private Validator validator;
    public UserModel user;

    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = getActivity().getLayoutInflater().inflate(R.layout.booking_dialog, null);
        initViews();
        initListeners();
        initPresenter();
        registerBus();
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //get arguments from bundle
        presenter.getArgsFromBundle();
        //final Spinner spinner = (Spinner) view.findViewById(R.id.spinnerType);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.booking_title);
        builder.setView(view)
                .setPositiveButton(R.string.agree_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // overrided  onStart()
                    }
                })
                .setNegativeButton(R.string.dismiss_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // overrided  onStart()
                    }
                });

        //setting the spinner
        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_spinner_item, BOOKING_TYPE);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterSpinner);
        //set text
        String textTimes = "";
        for (Integer i:times){
            textTimes = textTimes + (Integer.toString(i) + ":00, ");
        }
        textTimes = textTimes.substring(0, textTimes.length()-2); //cut space and comma
        tv.setText(textTimes);
        return builder.create();
    }

    @Override
    public void initViews() {
        ButterKnife.bind(this, view);
    }

    @Override
    public void initPresenter() {
        presenter = new BookingDialogPresenterImpl(this);
    }

    @Override
    public void setDate(String year, String month, String day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.chekIsAuth();
        //overrides OK button click
        AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    validator.validate();
                    if (validated){
                        presenter.book(year,
                                month,
                                day,
                                times,
                                nameEditText.getText().toString(),
                                phoneNumberEdit.getText().toString(),
                                commentEditText.getText().toString(),
                                Long.toString(spinner.getSelectedItemId()));
                    }
                    }
            });
        }
    }

    @Override
    public void setTimes(Vector<Integer> times) {
        this.times  = times;
    }

    @Override
    public void bookingSuccess() {
        parentView.onStart();
        Utils.showMessage(getContext(), "Время забронировано успешно!");
        //this.dismiss(); // dismmissing moved to presenter, after loading notifications
        presenter.updateNotifications();
    }

    @Override
    public void bookingFailure(String s) {
        Utils.showMessage(getContext(), s);
        onDataLoadingEnd(new HidePreloader());
    }

    @Override
    public void setParentView(BookingFragment bookingFragment) {
        this.parentView = bookingFragment;
    }

    @Override
    public void ifAuthed() {
        presenter.loadUser();

    }

    @Override
    public void ifNotAuthed() {

    }


    @Override
    public void onLoadUserSuccess(UserModel userModelValue) {
        this.user = userModelValue;
        nameEditText.setText(user.getFirstName());
        phoneNumberEdit.setMaskedText(user.getTelephone()
                .substring(3, user.getTelephone().length()));
    }

    @Override
    public void onLoadUserFailure(String error) {
        Utils.showMessage(getContext(), error);

    }

    @Override
    public void initListeners() {}

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        unRegisterBus();
        super.onDestroy();
    }


    @Override
    public void onValidationSucceeded() {
        validated = true;
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        validated = false;
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());
            // Display error messages
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Utils.showMessage(getContext(), message);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadingStart(ShowPreloader event) {
        sendBookingSpinKit.setVisibility(View.VISIBLE);
        allowCancelDialog(false, (AlertDialog) this.getDialog());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadingEnd(HidePreloader event) {
        sendBookingSpinKit.setVisibility(View.GONE);
        allowCancelDialog(true, (AlertDialog) this.getDialog());
    }

    @Override
    public void registerBus() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void unRegisterBus() {
        EventBus.getDefault().unregister(this);
    }
}
