package com.antipov.rehearsalbasementmvpv2.ui.dialog.booking;

import java.util.Vector;

/**
 * Created by antipov on 3/20/17.
 */
public interface BookingDialogPresenter {
    void onDestroy();

    void getArgsFromBundle();

    void book(String year, String month, String day, Vector<Integer> time, String name,
              String telephone, String comment, String type);

    void chekIsAuth();

    void loadUser();

    void updateNotifications();
}
