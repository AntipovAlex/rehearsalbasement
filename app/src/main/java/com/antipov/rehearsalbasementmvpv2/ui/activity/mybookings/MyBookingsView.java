package com.antipov.rehearsalbasementmvpv2.ui.activity.mybookings;

import com.antipov.rehearsalbasementmvpv2.ui.activity.base.BaseView;

/**
 * Created by antipov on 07.04.17.
 */

public interface MyBookingsView extends BaseView.View {
    void initPager();
}
