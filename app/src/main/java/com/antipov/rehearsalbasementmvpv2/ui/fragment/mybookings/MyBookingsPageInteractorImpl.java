package com.antipov.rehearsalbasementmvpv2.ui.fragment.mybookings;

import android.content.Context;

import com.antipov.rehearsalbasementmvpv2.API.BookingAPI;
import com.antipov.rehearsalbasementmvpv2.events.HidePreloader;
import com.antipov.rehearsalbasementmvpv2.events.ShowPreloader;
import com.antipov.rehearsalbasementmvpv2.model.response.BookingResponseModel;
import com.antipov.rehearsalbasementmvpv2.utils.RetrofitUtils;
import com.antipov.rehearsalbasementmvpv2.utils.SessionManager;

import org.greenrobot.eventbus.EventBus;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by antipov on 07.04.17.
 */

public class MyBookingsPageInteractorImpl implements MyBookingsPageInteractor {

    @Override
    public void loadData(Context context, String key, int currentPage, final OnDataLoad onDataLoadListener) {
        SessionManager sessionManager = new SessionManager(context);
        EventBus.getDefault().post(new ShowPreloader());
        final Observable<BookingResponseModel> call = RetrofitUtils.getApiBookingApiClient()
                .create(BookingAPI.class).getByKey(sessionManager.getToken(), key, currentPage);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BookingResponseModel>() {
                    @Override
                    public void onCompleted() {
                        EventBus.getDefault().post(new HidePreloader());
                    }

                    @Override
                    public void onError(Throwable e) {
                        onDataLoadListener.onDataLoadFailure(e.getMessage());
                        EventBus.getDefault().post(new HidePreloader());
                    }

                    @Override
                    public void onNext(BookingResponseModel bookingModel) {
                        try{
                            if (!bookingModel.getStatus()){
                                onError(new Throwable(bookingModel.getError()));
                            }
                            onDataLoadListener.onDataLoadSuccess(bookingModel.getData());
                        }catch (Throwable t){
                            onError(t);
                        }

                    }
                });
    }
}
