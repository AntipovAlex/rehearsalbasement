package com.antipov.rehearsalbasementmvpv2.ui.activity.navigation;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.antipov.rehearsalbasementmvpv2.R;
import com.antipov.rehearsalbasementmvpv2.model.UserModel;
import com.antipov.rehearsalbasementmvpv2.ui.activity.mybookings.MyBookingsActivity;
import com.antipov.rehearsalbasementmvpv2.ui.fragment.booking.BookingFragment;
import com.antipov.rehearsalbasementmvpv2.ui.fragment.calendar.CalendarFragment;
import com.antipov.rehearsalbasementmvpv2.utils.SessionManager;
import com.antipov.rehearsalbasementmvpv2.utils.Utils;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NavigationActivity extends AppCompatActivity implements NavigationView,
        NavigationView.onUserLoad, NavigationView.OnGetNotification {

    @BindView(R.id.hamburger_menu)
    ImageView hamburgerMenu;

    @BindView(R.id.badger)
    TextView badger;

    @BindView(R.id.navigation_header_text)
    TextView headerText;


    private Toolbar toolbar;
    private NavigationPresenter presenter;
    private Drawer.OnDrawerItemClickListener listener;
    private Drawer drawer;
    public UserModel user;
    private PrimaryDrawerItem myBookingsItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation2);
        initToolbar();
        initOnClickListeners(); //it must be called before initDrawer()
        initPresenter();
        initViews();
        setFragment(new CalendarFragment(), null);
        presenter.checkIsAuth();
    }

    @Override
    public void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void initDrawer(Boolean isAuthenticated) {
        if (isAuthenticated){ //making drawer for authenticated user
            AccountHeader headerResult = new AccountHeaderBuilder()
                    .withActivity(this)
                    .withHeaderBackground(R.drawable.header)
                    .withSelectionListEnabledForSingleProfile(false)
                    .withProfileImagesVisible(false)
                    .addProfiles(
                            new ProfileDrawerItem().withName(user.getFirstName()+" "+user.getLastName())
                                    .withEmail(user.getEmail())
                    )
                    .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                        @Override
                        public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                            return false;
                        }
                    })
                    .build();
            // primary item created as variable to be able for updating in future
            myBookingsItem = new PrimaryDrawerItem().withName("Моя бронь").withIcon(FontAwesome.Icon.faw_book)
                    .withBadge("").withBadgeStyle(new BadgeStyle().withTextColor(Color.WHITE)
                            .withColorRes(R.color.md_red_700)).withIdentifier(1);

            // generating drawer
            drawer = new DrawerBuilder()
                    .withActivity(this)
                    //.withToolbar(toolbar)
                    .withActionBarDrawerToggle(true)
                    .withAccountHeader(headerResult)
                    .addDrawerItems(
                            myBookingsItem,
                            new PrimaryDrawerItem().withName("Мои абонементы").withIcon(FontAwesome.Icon.faw_ticket),
                            new PrimaryDrawerItem().withName("Купить абонемент").withIcon(FontAwesome.Icon.faw_credit_card),
                            new SectionDrawerItem().withName("Дополнительно"),
                            new SecondaryDrawerItem().withName("Настройки").withIcon(FontAwesome.Icon.faw_cogs),
                            new SecondaryDrawerItem().withName("Справка").withIcon(FontAwesome.Icon.faw_question_circle),
                            new SecondaryDrawerItem().withName("Правила").withIcon(FontAwesome.Icon.faw_exclamation),
                            new SecondaryDrawerItem().withName("Контакты").withIcon(FontAwesome.Icon.faw_phone)
                    ).
                    withOnDrawerItemClickListener(listener)
                    .withSelectedItem(-1)
                    .build();
        } else {//making drawer for NON authenticated user
            drawer = new DrawerBuilder()
                    .withActivity(this)
                    //.withToolbar(toolbar)
                    .withActionBarDrawerToggle(true)
                    .withHeader(R.layout.drawer_header_unauth)
                    .addDrawerItems(
                            new PrimaryDrawerItem().withName("Моя бронь").withIcon(FontAwesome.Icon.faw_book)
                                    .withEnabled(false),
                            new PrimaryDrawerItem().withName("Мои абонементы").withIcon(FontAwesome.Icon.faw_ticket)
                                    .withEnabled(false),
                            new PrimaryDrawerItem().withName("Купить абонемент").withIcon(FontAwesome.Icon.faw_credit_card)
                                    .withEnabled(false),
                            new SectionDrawerItem().withName("Дополнительно"),
                            new SecondaryDrawerItem().withName("Настройки").withIcon(FontAwesome.Icon.faw_cogs)
                                    .withEnabled(false),
                            new SecondaryDrawerItem().withName("Справка").withIcon(FontAwesome.Icon.faw_question_circle)
                                    .withEnabled(false),
                            new SecondaryDrawerItem().withName("Правила").withIcon(FontAwesome.Icon.faw_exclamation)
                                    .withEnabled(false),
                            new SecondaryDrawerItem().withName("Контакты").withIcon(FontAwesome.Icon.faw_phone)
                                    .withEnabled(false),
                            new SecondaryDrawerItem().withName("Задать вопрос").withIcon(FontAwesome.Icon.faw_question)
                                    .withEnabled(false)
                    ).
                    withOnDrawerItemClickListener(listener)
                    .withSelectedItem(-1)
                    .build();
        }
    }

    @OnClick(R.id.hamburger_menu)
    void onMenuClick(){
        drawer.openDrawer();
    }

    @Override
    public void initPresenter() {
        this.presenter = new NavigationPresenterImpl(this);
    }

    @Override
    public void initViews() {
        ButterKnife.bind(this);
    }

    @Override
    public void initOnClickListeners() {
        listener = new Drawer.OnDrawerItemClickListener() {
            @Override
            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                if (position == 1){
                    goToMyBookings();
                }
                return false;
            }
        };
    }

    private void goToMyBookings() {
        Intent intent = new Intent(this, MyBookingsActivity.class);
        startActivity(intent);
    }

    @Override
    public void setFragment(Fragment fragment, Bundle args) {
        //if we passing args (bundle isnt null)
        if (args != null){
            fragment.setArguments(args);
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        // добавляем фрагмент
        fragmentTransaction.replace(R.id.navigationFragmentContainer, fragment);
        if (fragment instanceof BookingFragment){ //if we must go to booking fragment - add this to backstack
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }

    @Override
    public void ifAuthed() {
        presenter.loadUser();
    }

    @Override
    public void ifNotAuthed() {
        initDrawer(false);
    }

    @Override
    public void setHeaderText(String header) {
        this.headerText.setText(header);
    }


    @Override
    protected void onStart() {
        super.onStart();
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        //updating notifs each time when activity becomes visible to user
        if (sessionManager.isLoggedIn()){
            presenter.getNotifications();
        }
    }

    @Override
    public void onUserLoadSucces(UserModel user) {
        this.user = user;
        initDrawer(true);
    }

    @Override
    public void onUserLoadFail(String err) {
        Utils.showMessage(getApplicationContext(), err);
        initDrawer(false);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onGetNotificationSuccess(int data) {
        updateNotifications(data);
    }

    private void updateNotifications(int data) {
        if (data == 0) {
            // if data = 0 hiding badge
            myBookingsItem.withBadge((String) null);
            badger.setVisibility(View.INVISIBLE);
        } else {
            myBookingsItem.withBadge(data + "");
            badger.setVisibility(View.VISIBLE);
            badger.setText(data + "");
        }
        drawer.updateItem(myBookingsItem);
    }

    @Override
    public void onGetNotificationFailure(String message) {
        Utils.showMessage(getApplicationContext(), message);
    }


}
