package com.antipov.rehearsalbasementmvpv2.ui.activity.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.antipov.rehearsalbasementmvpv2.R;
import com.antipov.rehearsalbasementmvpv2.model.UserModel;
import com.antipov.rehearsalbasementmvpv2.ui.activity.navigation.NavigationActivity;
import com.antipov.rehearsalbasementmvpv2.ui.dialog.autorization.AutorizationFragment;
import com.antipov.rehearsalbasementmvpv2.ui.dialog.registration.RegistrationFragment;
import com.antipov.rehearsalbasementmvpv2.utils.SessionManager;
import com.antipov.rehearsalbasementmvpv2.utils.Utils;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
;import static android.view.View.GONE;

public class MainActivity extends AppCompatActivity implements MainView, MainView.LoginListener,
        MainView.RegisterListener, MainView.UserLoadingListener{

    @BindView(R.id.loggedLayout)
    View loggedLayout;

    @BindView(R.id.notLoggedLayout)
    View notLoggedLayout;

    @BindView(R.id.textGreetingsName)
    TextView greetingsNameText;

    MainPresenter presenter;
    UserModel user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initPresenter();
        initViews();
        presenter.checkIsAuthed();
    }

    @Override
    public void initPresenter() {
        this.presenter = new MainPresenterImpl(this);
    }

    @Override
    public void initViews() {
        ButterKnife.bind(this);
    }

    @Override
    public void goCalendar() {
        Intent intent = new Intent(this, NavigationActivity.class);
        startActivity(intent);
    }

    @Override
    public void showRegistration() {
        RegistrationFragment registrationDialog = new RegistrationFragment();
        registrationDialog.serParentView(this);
        registrationDialog.show(getSupportFragmentManager(), "RegistrationDialog");
    }

    @Override
    public void showAutorization() {
        AutorizationFragment autorizationDialog = new AutorizationFragment();
        autorizationDialog.setParentView(this); //passing instance of that activity to presenter and other stuff
        autorizationDialog.show(getSupportFragmentManager(), "AutorizationDialog");
    }

    @Override
    public void showNotLoggedInLayout() {
        loggedLayout.setVisibility(GONE);
        notLoggedLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoggedInLayout() {
        notLoggedLayout.setVisibility(GONE);
        loggedLayout.setVisibility(View.VISIBLE);
        greetingsNameText.setText(user.getFirstName()+" "+user.getLastName());
        Utils.showMessage(getApplicationContext(), "Succes");
    }

    @Override
    public void onLoginSuccess() {
        presenter.loadUser();
    }

    @Override
    public void onLoginError(String error) {
        Utils.showMessage(getApplicationContext(), error);
    }


    @Override
    public void onUserLoadingSuccess() {
        showLoggedInLayout();
    }

    @Override
    public void onUserLoadingFailure(String error) {
        Utils.showMessage(getApplicationContext(), error);
        showNotLoggedInLayout();
    }

    @Override
    public void onRegisterSuccess() {
        Utils.showMessage(getApplicationContext(), "Вы успешно зарегистрировались!");
    }

    @Override
    public void onRegisterFailure(String error) {
        Utils.showMessage(getApplicationContext(), error);
    }

    @Override
    public void ifAuthed() {
        presenter.loadUser();
    }

    @Override
    public void ifNotAuthed() {
        showNotLoggedInLayout();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    // multiple binding not working :(
    @OnClick(R.id.textGoWithoutRegister)
    public void onGoWithoutRegisterClick(){
        goCalendar();
    }
    @OnClick(R.id.textGoRegister)
    public void onGoRegistredClick(){
        goCalendar();
    }


    @OnClick(R.id.textSwitchLanguage)
    public void onSwitchLanguageClick(){
        Utils.showMessage(this.getApplicationContext(), "english");
    }

    @OnClick(R.id.buttonLogout)
    public void onButtonLogoutClick(){
        presenter.logout(getApplicationContext());
    }

    @OnClick(R.id.buttonRegister)
    public void onButtonRegisterClick(){
        showRegistration();
    }

    @OnClick(R.id.buttonAutorization)
    public void onButtonAutroizationClick(){
        showAutorization();
    }

}
