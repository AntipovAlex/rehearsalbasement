package com.antipov.rehearsalbasementmvpv2.ui.activity.navigation;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.antipov.rehearsalbasementmvpv2.model.UserModel;
import com.antipov.rehearsalbasementmvpv2.ui.activity.base.BaseView;

/**
 * Created by antipov on 3/19/17.
 */

public interface NavigationView extends BaseView.View {

    interface onUserLoad{
        void onUserLoadSucces(UserModel user);

        void onUserLoadFail(String err);
    }

    void initToolbar();

    void initDrawer(Boolean isAuthenticated);

    void initOnClickListeners();

    void setFragment(Fragment fragment, Bundle args);

    void ifAuthed();

    void ifNotAuthed();

    interface OnGetNotification{
        void onGetNotificationSuccess(int data);

        void onGetNotificationFailure(String message);
    }

    void setHeaderText(String header);

}
