package com.antipov.rehearsalbasementmvpv2.ui.fragment.mybookings;

import android.content.Context;

import com.antipov.rehearsalbasementmvpv2.model.BookingModel;

import java.util.List;

/**
 * Created by antipov on 07.04.17.
 */

public interface MyBookingsPageInteractor {
    void loadData(Context context, String key, int currentPage, OnDataLoad onDataLoadListener);

    interface OnDataLoad{
        void onDataLoadSuccess(List<BookingModel> bookingModel);

        void onDataLoadFailure(String message);
    }
}
