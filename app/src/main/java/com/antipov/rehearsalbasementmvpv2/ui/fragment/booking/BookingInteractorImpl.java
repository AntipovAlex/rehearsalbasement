package com.antipov.rehearsalbasementmvpv2.ui.fragment.booking;

import android.icu.util.TimeUnit;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.antipov.rehearsalbasementmvpv2.API.BookingAPI;
import com.antipov.rehearsalbasementmvpv2.events.HidePreloader;
import com.antipov.rehearsalbasementmvpv2.events.ShowPreloader;
import com.antipov.rehearsalbasementmvpv2.model.response.BookingResponseModel;
import com.antipov.rehearsalbasementmvpv2.utils.RetrofitUtils;

import org.greenrobot.eventbus.EventBus;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.antipov.rehearsalbasementmvpv2.utils.Constants.DATE_KEY;

/**
 * Created by antipov on 3/19/17.
 */

public class BookingInteractorImpl implements BookingInteractor {
    @Override
    public void getDateFromBundle(Fragment fragment, OnGetDateFromBundle onGetDateFromBundle) {
        Bundle args = fragment.getArguments();
        try {
            String date = args.getString(DATE_KEY);
            onGetDateFromBundle.onGetDateFromBundleSuccess(date);
        }catch (Exception e){
            onGetDateFromBundle.onGetDateFromBundleFailure();
        }
    }

    @Override
    public void loadBookings(String[] date, final OnLoadBookings onLoadBookings) {
        EventBus.getDefault().post(new ShowPreloader());
        final Observable<BookingResponseModel> call = RetrofitUtils.getApiBookingApiClient().
                create(BookingAPI.class).getForDay(date[2],date[1],date[0]);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BookingResponseModel>() {
                    @Override
                    public void onCompleted() {
                        EventBus.getDefault().post(new HidePreloader());
                    }

                    @Override
                    public void onError(Throwable e) {
                        onLoadBookings.onLoadBookingsFailure(e.toString());
                    }

                    @Override
                    public void onNext(BookingResponseModel bookingModel) {
                        try{
                            if (!bookingModel.getStatus()){
                                onError(new Throwable(bookingModel.getError()));
                            }
                            onLoadBookings.onLoadBookingsSuccess(bookingModel.getData());
                        } catch (Throwable t){
                            onError(t);
                        }

                    }
                });
    }
}
