package com.antipov.rehearsalbasementmvpv2.ui.dialog.autorization;

import com.antipov.rehearsalbasementmvpv2.events.HidePreloader;
import com.antipov.rehearsalbasementmvpv2.utils.SessionManager;

/**
 * Created by antipov on 16.03.17.
 */

public class AutorizationPresenterImpl implements AutorizationPresenter,
        AutorizationInteractor.LoginListener{
    private AutorizationFragment view;
    private AutorizationInteractorImpl interactor;

    public AutorizationPresenterImpl(AutorizationFragment autorizationFragment) {
        this.view = autorizationFragment;
        this.interactor = new AutorizationInteractorImpl();
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }

    @Override
    public void doLogin(String username, String password) {
        interactor.login(username,  password, view.getContext(), this);
    }

    @Override
    public void onLoginSuccess(String token) {
        //loginning user
        SessionManager sessionManager = new SessionManager(view.getContext());
        sessionManager.logInUser(token);
        view.parentView.onLoginSuccess();
        view.dismiss();
    }

    @Override
    public void onLoginFailure(String error) {
        SessionManager sessionManager = new SessionManager(view.getContext());
        sessionManager.removeSession();
        view.parentView.onLoginError(error);
    }
}
