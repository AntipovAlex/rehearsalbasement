package com.antipov.rehearsalbasementmvpv2.ui.activity.mybookings;

/**
 * Created by antipov on 07.04.17.
 */

public class MyBookingsPresenterImpl implements MyBookingsPresenter {
    MyBookingsActivity view;
    MyBookingsInteractorImpl interactor;

    MyBookingsPresenterImpl(MyBookingsActivity myBookingsActivity) {
        this.view = myBookingsActivity;
        this.interactor = new MyBookingsInteractorImpl();
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
