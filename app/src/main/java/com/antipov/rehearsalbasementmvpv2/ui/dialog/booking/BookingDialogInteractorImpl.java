package com.antipov.rehearsalbasementmvpv2.ui.dialog.booking;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.antipov.rehearsalbasementmvpv2.API.BookingAPI;
import com.antipov.rehearsalbasementmvpv2.events.HidePreloader;
import com.antipov.rehearsalbasementmvpv2.events.ShowPreloader;
import com.antipov.rehearsalbasementmvpv2.model.UserModel;
import com.antipov.rehearsalbasementmvpv2.model.response.ActionResponseModel;
import com.antipov.rehearsalbasementmvpv2.model.response.UserResponseModel;
import com.antipov.rehearsalbasementmvpv2.utils.RetrofitUtils;
import com.antipov.rehearsalbasementmvpv2.utils.SessionManager;

import org.greenrobot.eventbus.EventBus;

import java.util.Vector;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by antipov on 3/20/17.
 */

public class BookingDialogInteractorImpl implements BookingDialogInteractor {


    @Override
    public void getArgsFromBundle(Fragment fragment, OnGetArgsFromBundle onGetArgsFromBundle) {
        try {
            Bundle args = fragment.getArguments();
            String year = args.getString("year");
            String month = args.getString("month");
            String day = args.getString("day");
            Bundle times = args.getBundle("times");
            Vector<Integer> timesList = new Vector<>();
            for (int i = 0; i < times.size(); i++){
                Integer buf = Integer.parseInt(times.getString(Integer.toString(i)).split("\\.")[0]);
                timesList.add(buf);
            }
            onGetArgsFromBundle.onGetArgsFromBundleSucces(year, month, day, timesList);
        }catch (Exception e){
            onGetArgsFromBundle.onGetArgsFromBundleFailure();
        }
    }

    @Override
    public void book(Context context, String year, String month, String day, String time, String name,
                     String telephone, String comment, String type, final OnBook onBook) {
        EventBus.getDefault().post(new ShowPreloader());
        SessionManager sessionManager = new SessionManager(context);
        String cookies;
        if (sessionManager.isLoggedIn()){
            cookies = sessionManager.getToken();
        } else {
            cookies = "";
        }

        final Observable<ActionResponseModel> call = RetrofitUtils.getApiBookingApiClient()
                .create(BookingAPI.class).sendBooking(cookies, year, month, day, time,
                            name, telephone, comment, type);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ActionResponseModel>() {
                    @Override
                    public void onCompleted() {
                        EventBus.getDefault().post(new HidePreloader());
                    }

                    @Override
                    public void onError(Throwable e) {
                        onBook.onBookFailure(e.toString());
                    }

                    @Override
                    public void onNext(ActionResponseModel responseModel) {
                        // server cause return error message, but request exucting successfully
                        // throwing error in that case
                        try{
                            if (!responseModel.isSuccess()){
                                onBook.onBookFailure(responseModel.getError());
                                return;
                            }
                            onBook.onBookSuccess();
                        }catch (Throwable t){
                            onError(t);
                        }

                    }
                });


    }

    @Override
    public void loadUser(Context context, final OnLoadUser onLoadUserListener) {
        SessionManager sessionManager = new SessionManager(context);
        Observable<UserResponseModel> call = RetrofitUtils.getApiBookingApiClient().
                create(BookingAPI.class).getUserData(sessionManager.getToken());
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onLoadUserListener.onLoadUserFailure(e.toString());
                    }

                    @Override
                    public void onNext(UserResponseModel responseModel) {
                        onLoadUserListener.onLoadUserSucces((UserModel) responseModel.getData());
                    }
                });
    }

    @Override
    public void updateNotifications(Context context, final OnNotificationsUpdated onNotificationsUpdated) {
        SessionManager sessionManager = new SessionManager(context);
        Observable<ActionResponseModel> call = RetrofitUtils.getApiBookingApiClient().
                create(BookingAPI.class).getNotifications(sessionManager.getToken());
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ActionResponseModel>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onNotificationsUpdated.onNotificationUpdateFailure(e.getMessage());
                    }

                    @Override
                    public void onNext(ActionResponseModel actionResponseModel) {
                        if (!actionResponseModel.isSuccess()){
                            onError(new Throwable(actionResponseModel.getError()));
                            return;
                        }
                        onNotificationsUpdated.onNotificationUpdateSuccess(((Double)actionResponseModel.getData()).intValue());
                    }
                });
    }
}
