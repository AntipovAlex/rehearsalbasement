package com.antipov.rehearsalbasementmvpv2.ui.fragment.mybookings;

import com.antipov.rehearsalbasementmvpv2.model.BookingModel;
import com.antipov.rehearsalbasementmvpv2.ui.activity.base.BaseView;

import java.util.List;

/**
 * Created by antipov on 07.04.17.
 */

public interface MyBookingsPageView extends BaseView.View {
    void initRecycler();

    interface OnDataLoad{
        void onDataLoadSuccess(List<BookingModel> bookings);

        void onDataLoadFailure(String message);
    }

    interface BusLifecycle{
        void registerBus();

        void unRegisterBus();
    }

}
