package com.antipov.rehearsalbasementmvpv2.ui.dialog.registration;

import com.antipov.rehearsalbasementmvpv2.ui.activity.base.BaseView;
import com.antipov.rehearsalbasementmvpv2.ui.activity.main.MainActivity;

/**
 * Created by antipov on 17.03.17.
 */

public interface RegistrationView extends BaseView.View {
    void serParentView(MainActivity mainActivity);

    void initValidators();

    interface onRegisterListener{
        void onRegisterSuccess();

        void onRegisterFailure(String error);
    }

    interface BusLifecycle{
        void registerBus();

        void unRegisterBus();
    }
}
