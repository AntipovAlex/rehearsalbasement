package com.antipov.rehearsalbasementmvpv2.ui.fragment.mybookings;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.antipov.rehearsalbasementmvpv2.R;
import com.antipov.rehearsalbasementmvpv2.events.HidePreloader;
import com.antipov.rehearsalbasementmvpv2.events.ShowPreloader;
import com.antipov.rehearsalbasementmvpv2.model.BookingModel;
import com.antipov.rehearsalbasementmvpv2.ui.adapter.SectionAdapter;
import com.antipov.rehearsalbasementmvpv2.utils.Utils;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;

public class MyBookingsPageFragment extends Fragment implements MyBookingsPageView,
        MyBookingsPageView.OnDataLoad, MyBookingsPageView.BusLifecycle{
    private MyBookingsPagePresenterImpl presenter;
    private View view;
    private List<BookingModel> bookings = new ArrayList<>();
    private SectionedRecyclerViewAdapter sectionAdapter;
    private LinearLayoutManager layoutManager;
    private boolean isLoading;
    private int currentPage = 1;

    @BindView(R.id.mybookings_recycler)
    RecyclerView recyclerView;

    @BindView(R.id.text_havent_bookings)
    TextView haventBookingsText;

    @BindView(R.id.swipyrefreshlayout)
    SwipyRefreshLayout swipeRefreshLayout;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerBus();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.today_bookings_fragment, container, false);
        initViews();
        initPresenter();
        initListeners();
        initRecycler();
        return view;
    }

    private void initListeners() {
        swipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTTOM);
        swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                presenter.loadData(getArguments().getString("key"), currentPage);
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.loadData(getArguments().getString("key"), currentPage);
    }

    @Override
    public void onDataLoadSuccess(List<BookingModel> bookings) {
        if (bookings.size() != 0){
            haventBookingsText.setVisibility(View.GONE);
            this.bookings.addAll(bookings);
            addDataToAdapter(bookings);
            currentPage++;
            swipeRefreshLayout.setRefreshing(false);
            recyclerView.scrollToPosition(layoutManager.findLastCompletelyVisibleItemPosition()+3);
        } else if (currentPage == 1){
            haventBookingsText.setVisibility(View.VISIBLE);
        }

    }

    private void addDataToAdapter(List<BookingModel> bookings) {
        Set<String> headers = new LinkedHashSet<>();
        // get unique dates
        for (BookingModel i : bookings){
            headers.add(Utils.parseDate(i.getTime()));
        }
        // get pair - unique date => list with bookings
        for (String s:headers){
            List<BookingModel> inSection = new ArrayList<>();
            for (BookingModel b: bookings){
                if (s.equals(Utils.parseDate(b.getTime()))) {
                    inSection.add(b);
                }
            }
            sectionAdapter.addSection(new SectionAdapter(s, inSection, getContext()));
        }
        sectionAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDataLoadFailure(String message) {
        Utils.showMessage(getContext(), message);
        this.bookings = null;
    }

    @Override
    public void initRecycler() {
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        sectionAdapter = new SectionedRecyclerViewAdapter();
        recyclerView.setAdapter(sectionAdapter);
    }

    public static MyBookingsPageFragment newInstance(String key){
        MyBookingsPageFragment todayBookingsFragment = new MyBookingsPageFragment();
        // cause from static method i cant access class vars
        Bundle args = new Bundle();
        args.putString("key", key);
        todayBookingsFragment.setArguments(args);
        return todayBookingsFragment;
    }

    @Override
    public void initPresenter() {
        this.presenter = new MyBookingsPagePresenterImpl(this);
    }

    @Override
    public void initViews() {
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        unRegisterBus();
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadingStart(ShowPreloader event) {
        // handle data loading start
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadingEnd(HidePreloader event) {
       swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void registerBus() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void unRegisterBus() {
        EventBus.getDefault().unregister(this);
    }

}
