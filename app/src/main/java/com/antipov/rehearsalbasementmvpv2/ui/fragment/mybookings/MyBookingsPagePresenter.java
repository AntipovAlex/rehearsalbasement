package com.antipov.rehearsalbasementmvpv2.ui.fragment.mybookings;

import android.content.Context;

import com.antipov.rehearsalbasementmvpv2.ui.activity.base.BasePresenter;

/**
 * Created by antipov on 07.04.17.
 */

public interface MyBookingsPagePresenter extends BasePresenter.Presenter {
    void loadData(String key, int currentPage);
}
