package com.antipov.rehearsalbasementmvpv2.ui.dialog.autorization;

import com.antipov.rehearsalbasementmvpv2.ui.activity.base.BasePresenter;

/**
 * Created by antipov on 16.03.17.
 */

public interface AutorizationPresenter extends BasePresenter.Presenter {
    void doLogin(String username, String password);
}
