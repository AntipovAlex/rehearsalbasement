package com.antipov.rehearsalbasementmvpv2.ui.activity.main;

import android.content.Context;

import com.antipov.rehearsalbasementmvpv2.model.UserModel;

/**
 * Created by antipov on 16.03.17.
 */

public interface MainInteractor {
    void getUser(Context context, OnGetUser onGetUserListener);

    void logout(Context context, OnLogout onLogoutListener);

    interface OnGetUser {
        void onGetUserSuccess(UserModel user);

        void onGetUserFailure(String s);
    }

    interface OnLogout{
        void onLogoutSuccess();

        void onLogoutFailure();
    }
}
