package com.antipov.rehearsalbasementmvpv2.ui.activity.navigation;

import android.content.Context;

import com.antipov.rehearsalbasementmvpv2.model.UserModel;

/**
 * Created by antipov on 3/19/17.
 */

public interface NavigationInteractor {
    void loadUser(Context context, OnLoadUser onLoadUserListener);

    interface OnLoadUser{
        void onLoadUserSuccess(UserModel userModelValue);

        void onLoadUserFailure(String err);
    }

    void loadNotifications(Context context, OnLoadNotifications onLoadNotifications);
    interface OnLoadNotifications{
        void onLoadNotificationsSuccess(int data);

        void onLoadNotificationsFailure(String message);
    }
}
