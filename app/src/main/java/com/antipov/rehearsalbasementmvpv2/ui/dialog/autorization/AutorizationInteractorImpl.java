package com.antipov.rehearsalbasementmvpv2.ui.dialog.autorization;

import android.content.Context;

import com.antipov.rehearsalbasementmvpv2.API.BookingAPI;
import com.antipov.rehearsalbasementmvpv2.events.HidePreloader;
import com.antipov.rehearsalbasementmvpv2.events.ShowPreloader;
import com.antipov.rehearsalbasementmvpv2.model.response.ActionResponseModel;
import com.antipov.rehearsalbasementmvpv2.utils.DeviceTokenManager;
import com.antipov.rehearsalbasementmvpv2.utils.RetrofitUtils;

import org.greenrobot.eventbus.EventBus;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by antipov on 16.03.17.
 *
 *
 */

public class AutorizationInteractorImpl implements AutorizationInteractor {

    @Override
    public void login(String username, String password, Context context,
                      final LoginListener loginListener) {
        EventBus.getDefault().post(new ShowPreloader()); //send event

        DeviceTokenManager deviceTokenManager = new DeviceTokenManager(context);

        final Observable<ActionResponseModel> call = RetrofitUtils.getApiClientWithInterceptor(context).
                create(BookingAPI.class).login(username, password, deviceTokenManager.getDeviceToken());
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ActionResponseModel>() {
                    @Override
                    public void onCompleted() {
                        EventBus.getDefault().post(new HidePreloader());
                    }

                    @Override
                    public void onError(Throwable e) {
                        loginListener.onLoginFailure(e.getMessage());
                        EventBus.getDefault().post(new HidePreloader());
                    }

                    @Override
                    public void onNext(ActionResponseModel responseModel) {
                        try{
                            if (!responseModel.isSuccess()){
                                onError(new Throwable(responseModel.getError()));
                                return;
                            }
                            loginListener.onLoginSuccess((String) responseModel.getData());
                        } catch (Throwable t){
                            onError(t);
                        }

                    }

                    public void error(){

                    }
                });


    }
}
