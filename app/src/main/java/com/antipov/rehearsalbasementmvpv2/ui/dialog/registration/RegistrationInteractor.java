package com.antipov.rehearsalbasementmvpv2.ui.dialog.registration;

/**
 * Created by antipov on 17.03.17.
 */

public interface RegistrationInteractor {
    void registration(String username, String email, String password, String firstname,
                      String lastname, String phone, OnRegistration onRegistration);

    interface OnRegistration{
        void onRegistrationSucces();

        void onRegistrationFailure(String errors);
    }
}
