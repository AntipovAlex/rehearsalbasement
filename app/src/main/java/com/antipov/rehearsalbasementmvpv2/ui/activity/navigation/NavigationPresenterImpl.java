package com.antipov.rehearsalbasementmvpv2.ui.activity.navigation;

import com.antipov.rehearsalbasementmvpv2.model.UserModel;
import com.antipov.rehearsalbasementmvpv2.utils.SessionManager;

/**
 * Created by antipov on 3/19/17.
 */

public class NavigationPresenterImpl implements NavigationPresenter, NavigationInteractor.OnLoadUser,
NavigationInteractor.OnLoadNotifications{
    private NavigationInteractor interactor;
    private NavigationActivity view;


    public NavigationPresenterImpl(NavigationActivity view) {
        this.view = view;
        this.interactor = new NavigationInteractorImpl();
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }

    @Override
    public void checkIsAuth() {
        SessionManager sessionManager = new SessionManager(view.getApplicationContext());
        if (sessionManager.isLoggedIn()){
            view.ifAuthed();
        } else {
            view.ifNotAuthed();
        }
    }

    @Override
    public void loadUser() {
        interactor.loadUser(view.getApplicationContext(), this);
    }

    @Override
    public void getNotifications() {
        interactor.loadNotifications(view.getApplicationContext(), this);
    }

    @Override
    public void onLoadUserSuccess(UserModel user) {
        view.onUserLoadSucces(user);
    }

    @Override
    public void onLoadUserFailure(String err) {
        view.onUserLoadFail(err);
    }

    @Override
    public void onLoadNotificationsSuccess(int data) {
        view.onGetNotificationSuccess(data);
    }

    @Override
    public void onLoadNotificationsFailure(String message) {
        view.onGetNotificationFailure(message);
    }
}
