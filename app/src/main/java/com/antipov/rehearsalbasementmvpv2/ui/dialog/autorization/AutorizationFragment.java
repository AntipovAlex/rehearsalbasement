package com.antipov.rehearsalbasementmvpv2.ui.dialog.autorization;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.antipov.rehearsalbasementmvpv2.R;
import com.antipov.rehearsalbasementmvpv2.events.HidePreloader;
import com.antipov.rehearsalbasementmvpv2.events.ShowPreloader;
import com.antipov.rehearsalbasementmvpv2.ui.activity.main.MainActivity;
import com.github.ybq.android.spinkit.SpinKitView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.antipov.rehearsalbasementmvpv2.utils.Utils.allowCancelDialog;

/**
 * Created by antipov on 16.03.17.
 */

public class AutorizationFragment  extends DialogFragment implements AutorizationView,
        AutorizationView.BusLifecycle {
    @BindView(R.id.editAutorizationUsername)
    EditText usernameEdit;

    @BindView(R.id.editAutorizationPassword)
    EditText passwordEdit;

    @BindView(R.id.autorization_spin_kit)
    SpinKitView autorizationSpinKit;

    MainActivity parentView;
    View view;
    private AutorizationPresenterImpl presenter;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.autorization_button);
        builder.setView(view)
                .setPositiveButton(R.string.autorization_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                            // overrided

                    }
                })
                .setNegativeButton(R.string.dismiss_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AutorizationFragment .this.getDialog().cancel();
                    }
                });

        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        //overrides OK button click
        AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    presenter.doLogin(usernameEdit.getText().toString(),
                            passwordEdit.getText().toString());

                }
            });
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = getActivity().getLayoutInflater().inflate(R.layout.autorization_dialog, null);
        initViews();
        initPresenter();
        registerBus();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        unRegisterBus();
        super.onDestroy();
    }

    @Override
    public void setParentView(MainActivity parentView) {
        this.parentView = parentView;
    }

    @Override
    public void initViews() {
        ButterKnife.bind(this, view);
    }

    @Override
    public void initPresenter() {
        this.presenter = new AutorizationPresenterImpl(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadingStart(ShowPreloader event) {
        autorizationSpinKit.setVisibility(View.VISIBLE);
        allowCancelDialog(false, (AlertDialog) this.getDialog());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadingEnd(HidePreloader event) {
        autorizationSpinKit.setVisibility(View.GONE);
        allowCancelDialog(true, (AlertDialog) this.getDialog());
    }

    @Override
    public void registerBus() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void unRegisterBus() {
        EventBus.getDefault().unregister(this);
    }
}
