package com.antipov.rehearsalbasementmvpv2.ui.fragment.calendar;

/**
 * Created by antipov on 3/19/17.
 */

public interface CalendarView {
    void initViews();

    void initListeners();

    void goToBooking();
}
