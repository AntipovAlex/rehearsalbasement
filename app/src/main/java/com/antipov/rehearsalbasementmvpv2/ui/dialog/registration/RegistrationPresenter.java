package com.antipov.rehearsalbasementmvpv2.ui.dialog.registration;

import com.antipov.rehearsalbasementmvpv2.ui.activity.base.BasePresenter;

/**
 * Created by antipov on 17.03.17.
 */

public interface RegistrationPresenter extends BasePresenter.Presenter{
    void doRegistration(String username, String email, String password, String firstname,
                        String lastname, String phone);
}
