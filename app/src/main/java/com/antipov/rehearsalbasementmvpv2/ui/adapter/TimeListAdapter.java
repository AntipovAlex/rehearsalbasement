package com.antipov.rehearsalbasementmvpv2.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.antipov.rehearsalbasementmvpv2.R;
import com.antipov.rehearsalbasementmvpv2.model.ListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antipov on 02.05.17.
 */

public class TimeListAdapter extends RecyclerView.Adapter<TimeListAdapter.ViewHolder> {
    private final Context context;
    private List<ListModel> objects; //items which be displayed
    private ArrayList<String> checkedObjects = new ArrayList<>();
    private int resource;

    //get checked objects
    public ArrayList<String> getCheckedObjects() {
        return checkedObjects;
    }

    //on check/uncheck checkbox listener
    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (buttonView.isChecked()){ //if checkbox is checked - adding in list
                checkedObjects.add((String) buttonView.getTag());
            }else{ //else removing
                checkedObjects.remove(checkedObjects.indexOf((String) buttonView.getTag()));
            }

        }
    };

    // card model
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView state;
        TextView time;
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            state = (TextView)itemView.findViewById(R.id.textBookingState);
            time = (TextView)itemView.findViewById(R.id.textBookingTime);
            checkBox = (CheckBox)itemView.findViewById(R.id.checkBoxIsBooked);
        }
    }

    // inflating layout and creating view holder
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.time_list_card, viewGroup, false);
        return new ViewHolder(itemView);
    }

    public TimeListAdapter(Context context, int resource, List<ListModel> objects) {
        this.context = context;
        this.objects = objects;
        this.resource = resource;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        //get object from list which passed into constructor
        ListModel listItem = objects.get(i);
        //setting holder values
        holder.time.setText(listItem.getTime());
        holder.state.setText(listItem.getState());
        holder.checkBox.setEnabled(listItem.isCheckboxEnabled());
        holder.checkBox.setText(listItem.getCheckboxText());
        holder.checkBox.setTag(listItem.getTime());
        holder.checkBox.setOnCheckedChangeListener(onCheckedChangeListener); //set listener
        holder.state.setTextColor(listItem.getStateColor());
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }
}
