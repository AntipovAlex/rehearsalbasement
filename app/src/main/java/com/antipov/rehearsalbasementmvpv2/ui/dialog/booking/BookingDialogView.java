package com.antipov.rehearsalbasementmvpv2.ui.dialog.booking;

import com.antipov.rehearsalbasementmvpv2.model.UserModel;
import com.antipov.rehearsalbasementmvpv2.ui.activity.base.BaseView;
import com.antipov.rehearsalbasementmvpv2.ui.fragment.booking.BookingFragment;

import java.util.Vector;

/**
 * Created by antipov on 3/20/17.
 */
public interface BookingDialogView extends BaseView.View{

    void initListeners();

    void setDate(String year, String month, String day);

    void setTimes(Vector<Integer> times);

    void setParentView(BookingFragment bookingFragment);

    void ifAuthed();

    void ifNotAuthed();

    interface BusLifecycle{
        void registerBus();

        void unRegisterBus();
    }

    interface onBookingSend{
        void bookingSuccess();

        void bookingFailure(String error);
    }

    interface onLoadUser{
        void onLoadUserSuccess(UserModel user);

        void onLoadUserFailure(String error);
    }


}
