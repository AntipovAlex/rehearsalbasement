package com.antipov.rehearsalbasementmvpv2.ui.fragment.booking;



import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.antipov.rehearsalbasementmvpv2.model.BookingModel;
import com.antipov.rehearsalbasementmvpv2.model.ListModel;
import com.antipov.rehearsalbasementmvpv2.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.antipov.rehearsalbasementmvpv2.utils.Constants.BOOKED;
import static com.antipov.rehearsalbasementmvpv2.utils.Constants.NOTBOOKED;
import static com.antipov.rehearsalbasementmvpv2.utils.Constants.TIMES_TO_DISPLAY;

/**
 * Created by antipov on 3/19/17.
 */

public class BookingPresenterImpl implements BookingPresenter,
        BookingInteractor.OnGetDateFromBundle,
        BookingInteractor.OnLoadBookings{
    private BookingFragment view;
    private BookingInteractorImpl interactor;

    public BookingPresenterImpl(BookingFragment bookingFragment) {
        this.view = bookingFragment;
        this.interactor = new BookingInteractorImpl();
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void getDataFromBundle() {
        interactor.getDateFromBundle(view, this);
    }

    @Override
    public void loadBookings(String date) {
        interactor.loadBookings(date.split("\\."), this);
    }

    @Override
    public ArrayList<ListModel> makeDataForeDisplay(List<BookingModel> bookings){
        ArrayList<String> timeToBeDisplayed = new ArrayList<>(); //list of times which will be displayed on the view
        ArrayList<ListModel> arrayList = new ArrayList<>();
        //getting data for timeToBeDisplayed
        for(BookingModel booking : bookings){
//            SimpleDateFormat df = new SimpleDateFormat("MMM d, yyyy h:mm:ss a", Locale.US);
            SimpleDateFormat format = new SimpleDateFormat("HH.mm", Locale.US);
            //format.setTimeZone(TimeZone.getTimeZone("GMT+3:00"));
            Date d = new Date(Long.valueOf(booking.getTime()));
            String formatted = format.format(d);
            timeToBeDisplayed.add(formatted);
        }
        //initializing list with data
        for (String i : TIMES_TO_DISPLAY) {
            if (timeToBeDisplayed.contains(i)){
                arrayList.add(new ListModel(i, "Недоступно", false, Color.parseColor("#9e3c3e") ,BOOKED));
            }else{
                arrayList.add(new ListModel(i, "Забронировать", true, Color.parseColor("#599E3C"), NOTBOOKED));
            }
        }
        return arrayList;

    }


    @Override
    public void onGetDateFromBundleSuccess(String date) {
        view.setDate(date);
    }

    @Override
    public void onGetDateFromBundleFailure() {
        Utils.showMessage(view.getContext(), "Failure during getting data from intent ...");
    }

    @Override
    public void onLoadBookingsSuccess(List<BookingModel> bookingModel) {
        view.onSuccessBookingLoad(bookingModel);
    }

    @Override
    public void onLoadBookingsFailure(String s) {
        view.onFailureBookingLoad(s);
    }
}
