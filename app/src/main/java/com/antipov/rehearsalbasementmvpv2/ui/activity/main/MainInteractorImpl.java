package com.antipov.rehearsalbasementmvpv2.ui.activity.main;

import android.content.Context;

import com.antipov.rehearsalbasementmvpv2.API.BookingAPI;
import com.antipov.rehearsalbasementmvpv2.model.response.UserResponseModel;
import com.antipov.rehearsalbasementmvpv2.utils.RetrofitUtils;
import com.antipov.rehearsalbasementmvpv2.utils.SessionManager;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by antipov on 16.03.17.
 */

public class MainInteractorImpl implements MainInteractor {
    @Override
    public void getUser(Context context, final OnGetUser onGetUserListener) {

        SessionManager sessionManager = new SessionManager(context);
        Observable<UserResponseModel> call = RetrofitUtils.getApiBookingApiClient().
                create(BookingAPI.class).getUserData(sessionManager.getToken());
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onGetUserListener.onGetUserFailure(e.toString());
                    }

                    @Override
                    public void onNext(UserResponseModel responseModel) {
                        if (!responseModel.getStatus()){
                            onError(new Throwable(responseModel.getError()));
                        }
                        onGetUserListener.onGetUserSuccess(responseModel.getData());
                    }
                });

    }

    @Override
    public void logout(Context context, OnLogout onLogoutListener) {
        try{
            SessionManager sessionManager = new SessionManager(context);
            sessionManager.removeSession();
            onLogoutListener.onLogoutSuccess();
        }catch (Exception e){
            onLogoutListener.onLogoutFailure();
        }
    }
}
