package com.antipov.rehearsalbasementmvpv2.ui.activity.navigation;

import android.content.Context;

import com.antipov.rehearsalbasementmvpv2.API.BookingAPI;
import com.antipov.rehearsalbasementmvpv2.model.response.ActionResponseModel;
import com.antipov.rehearsalbasementmvpv2.model.response.UserResponseModel;
import com.antipov.rehearsalbasementmvpv2.utils.RetrofitUtils;
import com.antipov.rehearsalbasementmvpv2.utils.SessionManager;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by antipov on 3/19/17.
 */

public class NavigationInteractorImpl implements NavigationInteractor {
    @Override
    public void loadUser(Context context, final OnLoadUser onLoadUserListener) {
        SessionManager sessionManager = new SessionManager(context);
        Observable<UserResponseModel> call = RetrofitUtils.getApiBookingApiClient().
        create(BookingAPI.class).getUserData(sessionManager.getToken());
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserResponseModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onLoadUserListener.onLoadUserFailure(e.toString());
                    }

                    @Override
                    public void onNext(UserResponseModel responseModel) {
                        onLoadUserListener.onLoadUserSuccess(responseModel.getData());
                    }
                });

    }

    @Override
    public void loadNotifications(Context context, final OnLoadNotifications onLoadNotifications) {
        SessionManager sessionManager = new SessionManager(context);
        Observable<ActionResponseModel> call = RetrofitUtils.getApiBookingApiClient().
                create(BookingAPI.class).getNotifications(sessionManager.getToken());
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ActionResponseModel>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onLoadNotifications.onLoadNotificationsFailure(e.getMessage());
                    }

                    @Override
                    public void onNext(ActionResponseModel actionResponseModel) {
                        if (!actionResponseModel.isSuccess()){
                            onError(new Throwable(actionResponseModel.getError()));
                            return;
                        }
                        onLoadNotifications.onLoadNotificationsSuccess(((Double)actionResponseModel.getData()).intValue());
                    }
                });
    }
}
