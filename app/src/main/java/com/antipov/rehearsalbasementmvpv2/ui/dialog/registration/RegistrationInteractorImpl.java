package com.antipov.rehearsalbasementmvpv2.ui.dialog.registration;

import com.antipov.rehearsalbasementmvpv2.API.BookingAPI;
import com.antipov.rehearsalbasementmvpv2.events.HidePreloader;
import com.antipov.rehearsalbasementmvpv2.events.ShowPreloader;
import com.antipov.rehearsalbasementmvpv2.model.response.ActionResponseModel;
import com.antipov.rehearsalbasementmvpv2.utils.RetrofitUtils;

import org.greenrobot.eventbus.EventBus;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by antipov on 17.03.17.
 */

public class RegistrationInteractorImpl implements RegistrationInteractor {
    @Override
    public void registration(String username, String email, String password, String firstname,
                             String lastname, String phone, final OnRegistration onRegistration) {
        EventBus.getDefault().post(new ShowPreloader());
        Observable<ActionResponseModel> call = RetrofitUtils.getApiBookingApiClient()
                .create(BookingAPI.class).register(username, email, password, firstname, lastname,
                        phone);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ActionResponseModel>() {
                    @Override
                    public void onCompleted() {
                        EventBus.getDefault().post(new HidePreloader());
                    }

                    @Override
                    public void onError(Throwable e) {
                        onRegistration.onRegistrationFailure(e.getMessage());
                        EventBus.getDefault().post(new HidePreloader());
                    }

                    @Override
                    public void onNext(ActionResponseModel responseModel) {
                        try{
                            if (!responseModel.isSuccess()){
                                onError(new Throwable(responseModel.getError()));
                                return;
                            }
                            onRegistration.onRegistrationSucces();
                        } catch (Throwable t){
                            onError(t);
                        }

                    }
                });
    }
}
