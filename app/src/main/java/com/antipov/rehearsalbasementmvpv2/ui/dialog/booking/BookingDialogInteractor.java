package com.antipov.rehearsalbasementmvpv2.ui.dialog.booking;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.antipov.rehearsalbasementmvpv2.model.UserModel;

import java.util.Vector;

/**
 * Created by antipov on 3/20/17.
 */
public interface BookingDialogInteractor {
    void getArgsFromBundle(Fragment fragment, OnGetArgsFromBundle onGetArgsFromBundle);

    void book(Context context, String year, String month, String day, String time, String name,
              String telephone, String comment, String type, OnBook onBook);

    void loadUser(Context context, OnLoadUser onLoadUserListener);

    void updateNotifications(Context context, OnNotificationsUpdated onNotificationsUpdated);

    interface OnLoadUser{
        void onLoadUserSucces(UserModel user);

        void onLoadUserFailure(String s);
    }

    interface OnBook{
        void onBookSuccess();

        void onBookFailure(String error);
    }

    interface OnGetArgsFromBundle{
        void onGetArgsFromBundleSucces(String year, String month, String day, Vector<Integer> timesString);

        void onGetArgsFromBundleFailure();
    }

    interface OnNotificationsUpdated{
        void onNotificationUpdateSuccess(int i);

        void onNotificationUpdateFailure(String message);
    }
}
