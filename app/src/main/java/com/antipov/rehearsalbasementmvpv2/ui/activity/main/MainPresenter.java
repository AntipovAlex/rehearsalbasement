package com.antipov.rehearsalbasementmvpv2.ui.activity.main;

import android.content.Context;

import com.antipov.rehearsalbasementmvpv2.ui.activity.base.BasePresenter;

/**
 * Created by antipov on 16.03.17.
 */

public interface MainPresenter extends BasePresenter.Presenter {
    void loadUser();

    void logout(Context context);

    void checkIsAuthed();
}
