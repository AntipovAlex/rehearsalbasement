package com.antipov.rehearsalbasementmvpv2.ui.fragment.booking;

import android.support.v4.app.Fragment;

import com.antipov.rehearsalbasementmvpv2.model.BookingModel;

import java.util.List;

/**
 * Created by antipov on 3/19/17.
 */

public interface BookingInteractor {

    void getDateFromBundle(Fragment fragment, OnGetDateFromBundle onGetDateFromBundle);

    void loadBookings(String[] date, OnLoadBookings onLoadBookings);

    interface OnLoadBookings{
        void onLoadBookingsSuccess(List<BookingModel> bookingModel);

        void onLoadBookingsFailure(String s);

    }

    interface OnGetDateFromBundle{
        void onGetDateFromBundleSuccess(String date);

        void onGetDateFromBundleFailure();
    }
}
