package com.antipov.rehearsalbasementmvpv2.ui.activity.mybookings;

import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.antipov.rehearsalbasementmvpv2.R;
import com.antipov.rehearsalbasementmvpv2.ui.adapter.BookingsPagerAdapter;

public class MyBookingsActivity extends AppCompatActivity implements MyBookingsView {

    FragmentPagerAdapter adapterViewPager;
    ViewPager vpPager;
    private MyBookingsPresenterImpl presenter;
    private ViewPager.OnPageChangeListener pagerListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bookings);
        initPager();
        initPresenter();
        initViews();
    }

    @Override
    public void initPresenter() {
        this.presenter = new MyBookingsPresenterImpl(this);
    }

    @Override
    public void initViews() {}

    @Override
    public void initPager() {
        vpPager = (ViewPager) findViewById(R.id.vpPager);
        adapterViewPager = new BookingsPagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);
        vpPager.setCurrentItem(1);
        vpPager.addOnPageChangeListener(pagerListener);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }


}
