package com.antipov.rehearsalbasementmvpv2.ui.fragment.calendar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.antipov.rehearsalbasementmvpv2.R;
import com.antipov.rehearsalbasementmvpv2.ui.activity.navigation.NavigationActivity;
import com.antipov.rehearsalbasementmvpv2.ui.activity.navigation.NavigationView;
import com.antipov.rehearsalbasementmvpv2.ui.fragment.booking.BookingFragment;
import com.antipov.rehearsalbasementmvpv2.utils.Utils;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.antipov.rehearsalbasementmvpv2.utils.Constants.DATE_KEY;

/**
 * Created by antipov on 3/19/17.
 */

public class CalendarFragment extends Fragment implements CalendarView {
    @BindView(R.id.calendarView)
    MaterialCalendarView calendar;
    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_calendar, container, false);
        initViews();
        initListeners();
        setHeader();
        return view;
    }

    private void setHeader() {
        NavigationActivity activity = (NavigationActivity) getActivity();
        activity.setHeaderText("Выбор даты");
    }

    private void setCalendarListener(MaterialCalendarView calendar) {
        calendar.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                goToBooking();
            }
        });
    }

    @Override
    public void initViews() {
        ButterKnife.bind(this, view);
    }

    @Override
    public void initListeners() {
        setCalendarListener(calendar);
    }

    @Override
    public void goToBooking() {
        //set date format
        @SuppressLint("SimpleDateFormat") DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        //getting date in string
        String stringDate =
                df.format(((MaterialCalendarView)view.findViewById(R.id.calendarView)).
                        getSelectedDate().getDate());
        //put string date into extras and
        Bundle args = new Bundle();
        args.putString(DATE_KEY, stringDate);
        //calling parent method to replace the fragmnet
        ((NavigationView)getActivity()).setFragment(new BookingFragment(), args);
    }
}
