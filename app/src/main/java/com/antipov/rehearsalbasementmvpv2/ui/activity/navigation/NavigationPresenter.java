package com.antipov.rehearsalbasementmvpv2.ui.activity.navigation;

/**
 * Created by antipov on 3/19/17.
 */

public interface NavigationPresenter {
    void onDestroy();

    void checkIsAuth();

    void loadUser();

    void getNotifications();
}
