package com.antipov.rehearsalbasementmvpv2.ui.dialog.autorization;

import android.content.Context;

/**
 * Created by antipov on 16.03.17.
 */

public interface AutorizationInteractor {
    void login(String username, String password, Context context, LoginListener loginListener);

    interface LoginListener{
        void onLoginSuccess(String token);

        void onLoginFailure(String error);
    }
}
