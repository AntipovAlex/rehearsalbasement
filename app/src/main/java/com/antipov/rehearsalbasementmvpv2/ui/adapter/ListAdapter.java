package com.antipov.rehearsalbasementmvpv2.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.antipov.rehearsalbasementmvpv2.R;
import com.antipov.rehearsalbasementmvpv2.model.ListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antipov on 3/19/17.
 */

public class ListAdapter extends ArrayAdapter<ListModel> {
    //fields
    private Context context;
    private int resource;
    private List<ListModel> objects; //items which be displayed
    private ArrayList<String> checkedObjects = new ArrayList<>();
    //constructor
    public ListAdapter(Context context, int resource, List<ListModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.objects = objects;
        this.resource = resource;
    }
    //get checked objects
    public ArrayList<String> getCheckedObjects() {
        return checkedObjects;
    }

    //on check/uncheck checkbox listener
    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (buttonView.isChecked()){ //if checkbox is checked - adding in list
                checkedObjects.add((String) buttonView.getTag());
            }else{ //else removing
                checkedObjects.remove(checkedObjects.indexOf((String) buttonView.getTag()));
            }

        }
    };
    //view holder
    private static class Holder{
        TextView state;
        TextView time;
        CheckBox checkBox;
    }
    //overrided getView
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        Holder holder = new Holder();
        if (convertView == null){
            //get inflater
            LayoutInflater layoutInflater =
                    (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(resource, null);
            //filling holder fields
            holder.state = (TextView)view.findViewById(R.id.textBookingState);
            holder.time = (TextView)view.findViewById(R.id.textBookingTime);
            holder.checkBox = (CheckBox)view.findViewById(R.id.checkBoxIsBooked);
            view.setTag(holder);
        }
        else {
            holder = (Holder)view.getTag();
        }
        //get object from list which passed into constructor
        ListModel listItem = objects.get(position);
        //setting holder values
        holder.time.setText(listItem.getTime());
        holder.state.setText(listItem.getState());
        holder.checkBox.setEnabled(listItem.isCheckboxEnabled());
        holder.checkBox.setText(listItem.getCheckboxText());
        holder.checkBox.setTag(listItem.getTime());
        holder.checkBox.setOnCheckedChangeListener(onCheckedChangeListener); //set listener
        holder.state.setTextColor(listItem.getStateColor());
        return view;
    }
}
