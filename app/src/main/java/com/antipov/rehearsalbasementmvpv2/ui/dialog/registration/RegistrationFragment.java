package com.antipov.rehearsalbasementmvpv2.ui.dialog.registration;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.antipov.rehearsalbasementmvpv2.R;
import com.antipov.rehearsalbasementmvpv2.events.HidePreloader;
import com.antipov.rehearsalbasementmvpv2.events.ShowPreloader;
import com.antipov.rehearsalbasementmvpv2.ui.activity.main.MainActivity;
import com.antipov.rehearsalbasementmvpv2.ui.activity.main.MainView;
import com.antipov.rehearsalbasementmvpv2.ui.fragment.booking.BookingFragment;
import com.antipov.rehearsalbasementmvpv2.utils.Utils;
import com.github.pinball83.maskededittext.MaskedEditText;
import com.github.ybq.android.spinkit.SpinKitView;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.antipov.rehearsalbasementmvpv2.utils.Constants.REG_EXP_EMAIL;
import static com.antipov.rehearsalbasementmvpv2.utils.Constants.REG_EXP_PHONE;
import static com.antipov.rehearsalbasementmvpv2.utils.Utils.allowCancelDialog;

/**
 * Created by antipov on 17.03.17.
 */

public class RegistrationFragment extends DialogFragment implements RegistrationView,
        Validator.ValidationListener, RegistrationView.BusLifecycle, RegistrationView.onRegisterListener {
    @NotEmpty(message = "Поле обязательно для ввода")
    //use @Pattern instead of @Email, cause last marks valid mail as invalid
    @Pattern(regex = REG_EXP_EMAIL, message = "Введите валидную почту")
    @BindView(R.id.editEmail)
    EditText emailEdit;

    @Password(min = 6, message = "Пароль минимум 6 символов")
    @BindView(R.id.editPassword)
    EditText passwordEdit;

    @ConfirmPassword(message = "Пароли не совпадают")
    @BindView(R.id.editPasswordRetype)
    EditText passwordRetypeEdit;

    @NotEmpty(message = "Поле обязательно для ввода")
    @BindView(R.id.editUsername)
    EditText usernameEdit;

    @NotEmpty(message = "Поле обязательно для ввода")
    @BindView(R.id.editLastname)
    EditText lastNameEdit;

    @NotEmpty(message = "Поле обязательно для ввода")
    @BindView(R.id.editFirstname)
    EditText firstNameEdit;

    @Pattern(regex = REG_EXP_PHONE, message =
            "Поле должно быть заполнено в формате 380ХХХХХХХХХ")
    @BindView(R.id.editTextPhoneNumber)
    MaskedEditText phoneNumber;

    @BindView(R.id.register_spin_kit)
    SpinKitView registerSpinKit;

    MainActivity parentView;
    private View view;
    private RegistrationPresenterImpl presenter;
    private Validator validator;
    private Boolean validated = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = getActivity().getLayoutInflater().inflate(R.layout.registration_dialog, null);
        initViews();
        initPresenter();
        initValidators();
        registerBus();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view)
                .setPositiveButton(R.string.register_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //behavior is overrided
                    }
                })
                .setNegativeButton(R.string.dismiss_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        //overrides OK button click
        AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    validator.validate();
                    if(validated){
                        presenter.doRegistration(
                                usernameEdit.getText().toString(),
                                emailEdit.getText().toString(),
                                passwordEdit.getText().toString(),
                                firstNameEdit.getText().toString(),
                                lastNameEdit.getText().toString(),
                                phoneNumber.getText().toString()
                        );
                    }
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        unRegisterBus();
        super.onDestroy();
    }

    @Override
    public void initViews() {
        ButterKnife.bind(this, view);
    }

    @Override
    public void initPresenter() {
        this.presenter = new RegistrationPresenterImpl(this);
    }

    @Override
    public void serParentView(MainActivity mainActivity) {
        this.parentView = mainActivity;
    }

    @Override
    public void initValidators() {
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    public void onValidationSucceeded() {
        validated = true;
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        validated = false;
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());
            // Display error messages
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Utils.showMessage(getContext(), message);
            }
        }
    }

    @Override
    public void registerBus() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void unRegisterBus() {
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadingStart(ShowPreloader event) {
        registerSpinKit.setVisibility(View.VISIBLE);
        allowCancelDialog(false, (AlertDialog) this.getDialog());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadingEnd(HidePreloader event) {
        registerSpinKit.setVisibility(View.GONE);
        allowCancelDialog(true, (AlertDialog) this.getDialog());
    }

    @Override
    public void onRegisterSuccess() {
        dismiss();
        parentView.onRegisterSuccess();
    }

    @Override
    public void onRegisterFailure(String error) {
        parentView.onRegisterFailure(error);
    }
}
