package com.antipov.rehearsalbasementmvpv2.ui.fragment.booking;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.antipov.rehearsalbasementmvpv2.R;
import com.antipov.rehearsalbasementmvpv2.events.HidePreloader;
import com.antipov.rehearsalbasementmvpv2.events.ShowPreloader;
import com.antipov.rehearsalbasementmvpv2.model.BookingModel;
import com.antipov.rehearsalbasementmvpv2.ui.activity.navigation.NavigationActivity;
import com.antipov.rehearsalbasementmvpv2.ui.adapter.ListAdapter;
import com.antipov.rehearsalbasementmvpv2.ui.adapter.TimeListAdapter;
import com.antipov.rehearsalbasementmvpv2.ui.dialog.booking.BookingDialogFragment;
import com.antipov.rehearsalbasementmvpv2.utils.Utils;
import com.github.ybq.android.spinkit.SpinKitView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by antipov on 3/19/17.
 */

public class BookingFragment extends Fragment implements BookingView, BookingView.OnBookingLoad, BookingView.BusLifecycle{
    @BindView(R.id.bookingDateText)
    TextView textBookingDate;

    @BindView(R.id.booking_load_error_text)
    TextView textBookingLoadError;

    @BindView(R.id.booking_spin_kit)
    SpinKitView mainSpinKit;

    @BindView(R.id.recyclerTimeList)
    RecyclerView recyclerView;

    @BindView(R.id.showBookingDialogButton)
    Button showBookingDialogButton;

    View view;
    String dateToday;
    List<BookingModel> bookingsToday;
    private BookingPresenterImpl presenter;
    //private ListAdapter listAdapter;
    private TimeListAdapter timeListAdapter;
    private BookingDialogFragment bookingDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_booking, container, false);
        initPresenter();
        initViews();
        registerBus();
        setHeader();
        return view;
    }

    private void setHeader() {
        NavigationActivity activity = (NavigationActivity)getActivity();
        activity.setHeaderText("Выбор времени");
    }

    @Override
    public void onStart() {
        presenter.getDataFromBundle();
        presenter.loadBookings(dateToday);
        super.onStart();
    }

    @Override
    public void onSuccessBookingLoad(List<BookingModel> bookingModel) {
        this.bookingsToday = bookingModel;
        displayList();
    }

    @Override
    public void onFailureBookingLoad(String s) {
        Utils.showMessage(this.getContext(), s);
        EventBus.getDefault().post(new HidePreloader());
        textBookingLoadError.setText(s);
        textBookingLoadError.setVisibility(View.VISIBLE);
        showBookingDialogButton.setVisibility(View.INVISIBLE);
    }


    @Override
    public void initPresenter() {
        this.presenter = new BookingPresenterImpl(this);
    }

    @Override
    public void initViews() {
        ButterKnife.bind(this, view);
    }

    @Override
    public void setDate(String date) {
        textBookingDate.setText(date);
        dateToday = date;
    }

    @Override
    public void displayList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        timeListAdapter = new TimeListAdapter(this.getContext(), R.layout.bookings_list_card,
                presenter.makeDataForeDisplay(bookingsToday));
        recyclerView.setAdapter(timeListAdapter);
    }

    @OnClick(R.id.showBookingDialogButton)
    @Override
    public void showBookingDialog() {
        ArrayList<String> checkedItems;
        checkedItems = timeListAdapter.getCheckedObjects(); //get checked items
        // checkedItems = listAdapter.getCheckedObjects(); //get checked items
        if (checkedItems.size() > 0){
            bookingDialog = new BookingDialogFragment();
            bookingDialog.setParentView(this);
            bookingDialog.show(getFragmentManager(), "BookingDialog"); //creating new dialog
            Bundle args = new Bundle(); //bundle with all arguments
            Bundle times = new Bundle(); //bundle with time to bookings, that will be included in args
            for (int i = 0; i < checkedItems.size(); i++){ //put args into bundle
                times.putString(Integer.toString(i), checkedItems.get(i));
            }
            args.putBundle("times", times);
            args.putString("year", dateToday.split("\\.")[2]);
            args.putString("month", dateToday.split("\\.")[1]);
            args.putString("day", dateToday.split("\\.")[0]);
            bookingDialog.setArguments(args); //passing arguments into dialog through bundle
        }else{
            Utils.showMessage(getActivity().getApplicationContext(),
                    "Необходимо отметить, как минимум, один час для продолжения");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadingStart(ShowPreloader event) {
        mainSpinKit.setVisibility(View.VISIBLE);
        showBookingDialogButton.setEnabled(false);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataLoadingEnd(HidePreloader event) {
        mainSpinKit.setVisibility(View.GONE);
        showBookingDialogButton.setEnabled(true);
    }


    @Override
    public void onDestroy() {
        presenter.onDestroy();
        unRegisterBus();
        super.onDestroy();
    }
    @Override
    public void registerBus() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void unRegisterBus() {
        EventBus.getDefault().unregister(this);
    }


}
