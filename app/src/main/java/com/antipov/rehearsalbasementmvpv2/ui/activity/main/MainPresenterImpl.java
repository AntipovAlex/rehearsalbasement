package com.antipov.rehearsalbasementmvpv2.ui.activity.main;

import android.content.Context;

import com.antipov.rehearsalbasementmvpv2.model.UserModel;
import com.antipov.rehearsalbasementmvpv2.utils.SessionManager;
import com.antipov.rehearsalbasementmvpv2.utils.Utils;

/**
 * Created by antipov on 16.03.17.
 */

public class MainPresenterImpl implements MainPresenter, MainInteractor.OnGetUser, MainInteractor.OnLogout{
    private MainInteractor interactor;
    private MainActivity view;

    public MainPresenterImpl(MainActivity view) {
        this.view = view;
        this.interactor = new MainInteractorImpl();
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }

    @Override
    public void loadUser() {
        interactor.getUser(view.getApplicationContext(), this);
    }

    @Override
    public void logout(Context context) {
        interactor.logout(context, this);
    }

    @Override
    public void checkIsAuthed() {
        SessionManager sessionManager = new SessionManager(view.getApplicationContext());
        if (sessionManager.isLoggedIn()){
            view.ifAuthed();
        } else {
            view.ifNotAuthed();
        }
    }

    @Override
    public void onGetUserSuccess(UserModel user) {
        if (user != null) {
            view.user = user;
            view.onUserLoadingSuccess();
        }
    }

    @Override
    public void onGetUserFailure(String s) {
        //if jsessid expired - destroy user session
        SessionManager sessionManager = new SessionManager(view.getApplicationContext());
        sessionManager.removeSession();
        view.onUserLoadingFailure(s);
    }

    @Override
    public void onLogoutSuccess() {
        view.showNotLoggedInLayout();
    }

    @Override
    public void onLogoutFailure() {
        Utils.showMessage(view.getApplicationContext(), "Проблемы во время логаута");

    }
}
