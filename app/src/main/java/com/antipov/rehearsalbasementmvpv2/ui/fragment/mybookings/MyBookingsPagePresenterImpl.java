package com.antipov.rehearsalbasementmvpv2.ui.fragment.mybookings;

import com.antipov.rehearsalbasementmvpv2.model.BookingModel;

import java.util.List;

/**
 * Created by antipov on 07.04.17.
 */

public class MyBookingsPagePresenterImpl implements MyBookingsPagePresenter,
MyBookingsPageInteractor.OnDataLoad{
    private MyBookingsPageInteractorImpl interactor;
    private MyBookingsPageFragment view;

    public MyBookingsPagePresenterImpl(MyBookingsPageFragment myBookingsPageFragment) {
        this.view = myBookingsPageFragment;
        this.interactor = new MyBookingsPageInteractorImpl();
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }

    @Override
    public void loadData(String key, int currentPage) {
        interactor.loadData(view.getContext(), key, currentPage, this);
    }

    @Override
    public void onDataLoadSuccess(List<BookingModel> bookingModel) {
        view.onDataLoadSuccess(bookingModel);
    }

    @Override
    public void onDataLoadFailure(String message) {
        view.onDataLoadFailure(message);
    }
}
