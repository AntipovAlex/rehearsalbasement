package com.antipov.rehearsalbasementmvpv2.ui.dialog.autorization;
import com.antipov.rehearsalbasementmvpv2.ui.activity.base.BaseView;
import com.antipov.rehearsalbasementmvpv2.ui.activity.main.MainActivity;

/**
 * Created by antipov on 16.03.17.
 */

public interface AutorizationView extends BaseView.View {
    void setParentView(MainActivity parentView);

    interface BusLifecycle{
        void registerBus();

        void unRegisterBus();
    }
}
