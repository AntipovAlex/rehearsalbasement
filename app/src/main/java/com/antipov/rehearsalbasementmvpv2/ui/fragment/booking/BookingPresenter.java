package com.antipov.rehearsalbasementmvpv2.ui.fragment.booking;

import com.antipov.rehearsalbasementmvpv2.model.BookingModel;
import com.antipov.rehearsalbasementmvpv2.model.ListModel;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by antipov on 3/19/17.
 */

public interface BookingPresenter {
    void onDestroy();

    void getDataFromBundle();

    void loadBookings(String date);

    ArrayList<ListModel> makeDataForeDisplay(List<BookingModel> bookings);
}
