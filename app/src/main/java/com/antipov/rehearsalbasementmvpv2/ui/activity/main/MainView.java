package com.antipov.rehearsalbasementmvpv2.ui.activity.main;

import com.antipov.rehearsalbasementmvpv2.ui.activity.base.BaseView;

/**
 * Created by antipov on 16.03.17.
 */

public interface MainView extends BaseView.View {
    void goCalendar();

    void showRegistration();

    void showAutorization();

    void showNotLoggedInLayout();

    void showLoggedInLayout();

    interface LoginListener{
        void onLoginSuccess();

        void onLoginError(String error);
    }

    interface RegisterListener{
        void onRegisterSuccess();

        void onRegisterFailure(String error);
    }

    interface UserLoadingListener{
        void onUserLoadingSuccess();

        void onUserLoadingFailure(String error);
    }

    void ifAuthed();

    void ifNotAuthed();
}
