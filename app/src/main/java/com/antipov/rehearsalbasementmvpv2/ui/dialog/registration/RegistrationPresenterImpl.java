package com.antipov.rehearsalbasementmvpv2.ui.dialog.registration;

/**
 * Created by antipov on 17.03.17.
 */

public class RegistrationPresenterImpl implements RegistrationPresenter,
        RegistrationInteractorImpl.OnRegistration{
    private RegistrationFragment view;
    private RegistrationInteractorImpl interactor;

    public RegistrationPresenterImpl(RegistrationFragment registrationFragment) {
        this.view = registrationFragment;
        this.interactor  = new RegistrationInteractorImpl();
    }


    @Override
    public void onDestroy() {
        this.view = null;
    }

    @Override
    public void doRegistration(String username, String email, String password, String firstname,
                               String lastname, String phone) {
        interactor.registration(username, email, password, firstname, lastname, phone, this);
    }

    @Override
    public void onRegistrationSucces() {
        view.onRegisterSuccess();
    }

    @Override
    public void onRegistrationFailure(String error) {
        view.onRegisterFailure(error);
    }
}
