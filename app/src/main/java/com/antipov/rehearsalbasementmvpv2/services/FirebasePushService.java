package com.antipov.rehearsalbasementmvpv2.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.antipov.rehearsalbasementmvpv2.R;
import com.antipov.rehearsalbasementmvpv2.ui.activity.main.MainActivity;
import com.antipov.rehearsalbasementmvpv2.ui.activity.mybookings.MyBookingsActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by Antipov on 09.05.17.
 */

public class FirebasePushService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        sendNotification(remoteMessage.getData());

    }
    private void sendNotification(Map<String, String> messageBody) {
        Intent notificationIntent = new Intent(getApplicationContext(), MyBookingsActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                .setContentTitle(messageBody.get("title"))
                .setContentText(messageBody.get("content"))
                .setContentIntent(resultPendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(0, notification.build());


    }
}
