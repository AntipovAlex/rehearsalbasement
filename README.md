# Rehearsal Basement
An application for online time-booking.

### Hello!
I am glad that you are reading this text.
Let me introduce my application, this is "Rehearsal Basement" - an application for online time booking.
I developed this application as my bachelor project, for improving my skills in mobile client-server developing, and to put some works to my portfolio.
It's not just training work, this app will assist to my friend in his business. I plan to add some new features, make beautiful material design, and release it in *Google Play*.

### How it works?
First of all, this is a client-server application. Now you are reading the readme for client side. [Follow the link](https://bitbucket.org/AntipovAlex/rehearsalservermvc/) to see server project (which made with *Spring Boot*, *Spring Security*, *Tomcat*)

Functionality divided for two user-groups: authenticated and non-authnticated.
Non-authenticated users can only book the time and see available time.
Authenticated users have personal page, with history of bookings, e.g. past bookings, today bookings, bookings in future, notifications, push-messages about today bookings, and more useful stuff in future.

This app developed using *MVP* pattern (using [Antonio Leiva implementation](https://antonioleiva.com/mvp-android/)) with next libraries:

 - [Retrofit 2](http://square.github.io/retrofit/) - HTTP requests
 - [RxAndroid](https://github.com/ReactiveX/RxAndroid) data fetching (in pair with Retrofit 2)
 - [Event Bus](https://github.com/greenrobot/EventBus) - custom events handling
 - [Butterknife](http://jakewharton.github.io/butterknife/) - data binding
 - [Firebase cloud messaging](https://firebase.google.com/docs/cloud-messaging/) - push notifications
 - [Material Drawer](https://github.com/mikepenz/MaterialDrawer) - navigation drawer in material style
 - [Material Calendar](https://github.com/prolificinteractive/material-calendarview) - calendar in material style
 - [SpinKit](https://github.com/ybq/Android-SpinKit) - preloaders
 - [SwipyRefreshLayout](https://github.com/omadahealth/SwipyRefreshLayout) - better implementation of 'pull to refresh' layout
 - [Android Saripaar](https://github.com/ragunathjawahar/android-saripaar) - data validation
 - [Android-Iconics](https://github.com/mikepenz/Android-Iconics) - icons for nav drawer
 - [SectionedRecyclerViewAdapter](https://github.com/luizgrp/SectionedRecyclerViewAdapter) -
 better recycler view implementation with implemented sections

### Let's see the app!
Main screen:
Registration\authentication, or greetings\logout.

![main screen](http://images.vfl.ru/ii/1499098471/9cf8a87a/17801999_m.png)
![main](http://images.vfl.ru/ii/1499098473/232d3df9/17802000_m.png)

Calendar screen, screen, where user choose date to book
Authorized and non authorized user:

![calendar](http://images.vfl.ru/ii/1499098474/b94882b4/17802001_m.png)![calendar](http://images.vfl.ru/ii/1499098476/d09dbc58/17802002_m.png)

Time screen, screen, where user chose time and type to book

![time](http://images.vfl.ru/ii/1499098478/9f8dbfba/17802004_m.png)


Screen with bookings history, only for authenticated user.

![history](http://images.vfl.ru/ii/1499098503/0bb0e377/17802005_m.png)

Push-notification:

![notif](http://images.vfl.ru/ii/1499098503/58c0aff8/17802006_m.png)
## Thank you for reading!
Please, note: I'm working on it now, it's alfa-version, not even beta. Bye!